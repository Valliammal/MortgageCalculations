package com.mortgage.spring.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mortgage.spring.model.MortgageRateDetails;

/**
 * Handles requests for the mortgageloyee service.
 */
@Controller
public class MortgageController {
	
	private static final Logger logger = LoggerFactory.getLogger(MortgageController.class);
	
	//Map to store mortgage, ideally we should use database
	Map<Integer, MortgageRateDetails> mortgageData = new HashMap<Integer, MortgageRateDetails>();
	
	MortgageController() {

		MortgageRateDetails mortgage = new MortgageRateDetails();
		mortgage.setId(1);
		mortgage.setMaturityPeriod(1);
		mortgage.setInterestRate(33.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(1, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(2);
		mortgage.setMaturityPeriod(2);
		mortgage.setInterestRate(32.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(2, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(3);
		mortgage.setMaturityPeriod(3);
		mortgage.setInterestRate(31.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(3, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(4);
		mortgage.setMaturityPeriod(4);
		mortgage.setInterestRate(31.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(4, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(5);
		mortgage.setMaturityPeriod(5);
		mortgage.setInterestRate(30.5);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(5, mortgage);
		
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(6);
		mortgage.setMaturityPeriod(6);
		mortgage.setInterestRate(29.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(6, mortgage);
		
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(7);
		mortgage.setMaturityPeriod(7);
		mortgage.setInterestRate(28.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(7, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(8);
		mortgage.setMaturityPeriod(8);
		mortgage.setInterestRate(27.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(8, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(9);
		mortgage.setMaturityPeriod(9);
		mortgage.setInterestRate(26.5);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(9, mortgage);

		
		mortgage = new MortgageRateDetails();
		mortgage.setId(10);
		mortgage.setMaturityPeriod(10);
		mortgage.setInterestRate(25.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(10, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(11);
		mortgage.setMaturityPeriod(11);
		mortgage.setInterestRate(24.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(11, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(12);
		mortgage.setMaturityPeriod(12);
		mortgage.setInterestRate(23.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(12, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(13);
		mortgage.setMaturityPeriod(13);
		mortgage.setInterestRate(22.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(13, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(14);
		mortgage.setMaturityPeriod(14);
		mortgage.setInterestRate(21.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(14, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(15);
		mortgage.setMaturityPeriod(15);
		mortgage.setInterestRate(20.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(15, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(16);
		mortgage.setMaturityPeriod(16);
		mortgage.setInterestRate(19.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(16, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(17);
		mortgage.setMaturityPeriod(17);
		mortgage.setInterestRate(18.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(17, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(18);
		mortgage.setMaturityPeriod(18);
		mortgage.setInterestRate(17.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(18, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(19);
		mortgage.setMaturityPeriod(19);
		mortgage.setInterestRate(16.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(19, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(20);
		mortgage.setMaturityPeriod(20);
		mortgage.setInterestRate(15.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(20, mortgage);

		
		mortgage = new MortgageRateDetails();
		mortgage.setId(21);
		mortgage.setMaturityPeriod(21);
		mortgage.setInterestRate(14.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(21, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(22);
		mortgage.setMaturityPeriod(22);
		mortgage.setInterestRate(13.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(22, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(23);
		mortgage.setMaturityPeriod(23);
		mortgage.setInterestRate(12.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(23, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(24);
		mortgage.setMaturityPeriod(24);
		mortgage.setInterestRate(11.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(24, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(25);
		mortgage.setMaturityPeriod(25);
		mortgage.setInterestRate(10.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(25, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(26);
		mortgage.setMaturityPeriod(26);
		mortgage.setInterestRate(9.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(26, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(27);
		mortgage.setMaturityPeriod(27);
		mortgage.setInterestRate(8.5);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(27, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(28);
		mortgage.setMaturityPeriod(28);
		mortgage.setInterestRate(7.95);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(28, mortgage);
		
		mortgage = new MortgageRateDetails();
		mortgage.setId(29);
		mortgage.setMaturityPeriod(29);
		mortgage.setInterestRate(7.85);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(29, mortgage);

		mortgage = new MortgageRateDetails();
		mortgage.setId(30);
		mortgage.setMaturityPeriod(30);
		mortgage.setInterestRate(7.75);
		mortgage.setLastUpdate(new Date());
		mortgageData.put(30, mortgage);

		
	}
		
	//income (Amount), maturityPeriod (integer), loanValue (Amount), homeValue (Amount)
	@RequestMapping(value = MortgageRestURIConstants.CALCULATE_MORTGAGE_DETAILS, method = RequestMethod.GET)
	public @ResponseBody String getMortgageDetails(
			@RequestParam("income") double income,
			@RequestParam("maturityPeriod") int maturityPeriod, 
			@RequestParam("loanValue") double loanValue, 
			@RequestParam("homeValue") double homeValue) {
		
		Boolean mortgageYesNo= false;
		logger.info("Start " );
		logger.info("Income " + income);
		logger.info("MaturityPeriod " + maturityPeriod);
		logger.info("loanValue " +  loanValue);
		logger.info("homeValue " + homeValue);
		
		// if the maturityPeriod is that much, then corresponding calc
		//a mortgage should not exceed 4 times the income
		//a mortgage should not exceed the home value
		
		String message = "";
		if ((loanValue > homeValue) || (loanValue > (4 * income))) {
			mortgageYesNo= false;
			message =  "Mortgage will not be given " + mortgageYesNo + "no Monthly cost ";
		} else {
			
			mortgageYesNo= true;
			MortgageRateDetails mortgage = mortgageData.get(maturityPeriod);
			logger.info("Data in the Mortgage Flow Id" +  mortgage.getId());
			logger.info("Data in the Mortgage Flow InterestRate" +  mortgage.getInterestRate());
			logger.info("Data in the Mortgage Flow Maturity period " +  mortgage.getMaturityPeriod());
			logger.info("Data in the Mortgage Flow Last Update date " +  mortgage.getLastUpdate());

			maturityPeriod = maturityPeriod * 12;
			double rate = mortgage.getInterestRate();
			rate = rate/100/12; 
		    double payment = (loanValue * rate) / (1 - Math.pow(1 + rate, -maturityPeriod));
		    // round to two decimals
		    payment = (double)Math.round(payment * 100) / 100;
		    System.out.println("Payment: " + payment);
			message =  "Mortgage will  be given " + mortgageYesNo + " Monthly cost " + payment;
		}

		// MonthlyCost Of the Mortgage also
		return message;
	}
	
	@RequestMapping(value = MortgageRestURIConstants.GET_MORTGAGE_RATE, method = RequestMethod.GET)
	public @ResponseBody MortgageRateDetails getmortgageRateDetails(@PathVariable("id") int mortgageId) {
		logger.info("Start getmortgageloyee. ID="+mortgageId);
		return mortgageData.get(mortgageId);
	}
	
	@RequestMapping(value = MortgageRestURIConstants.GET_ALL_MORTGAGE_RATES, method = RequestMethod.GET)
	public @ResponseBody List<MortgageRateDetails> getAllmortgageInterestRates() {

		logger.info("Start Get ALL Mortgage Interest Rates.");
		List<MortgageRateDetails> mortgages = new ArrayList<MortgageRateDetails>();
		
		Set<Integer> mortgageIdKeys = mortgageData.keySet();
		for(Integer i : mortgageIdKeys){
			mortgages.add(mortgageData.get(i));
		}
		return mortgages;
		
	}
	
	@RequestMapping(value = MortgageRestURIConstants.CREATE_MORTGAGE_RATE, method = RequestMethod.POST)
	public @ResponseBody MortgageRateDetails createmortgageRates(
			@PathVariable("id") int id,
			@RequestParam("lastUpdate") Date lastUpdate,
			@RequestParam("maturityPeriod") int maturityPeriod, 
			@RequestParam("loanValue") double loanValue, 
			@RequestParam("interestRate") double interestRate) {

		logger.info("Start createmortgageloyee.");
		MortgageRateDetails mortgage = new MortgageRateDetails();
		mortgage.setLastUpdate(lastUpdate);
		mortgage.setMaturityPeriod(maturityPeriod);
		mortgage.setInterestRate(interestRate);
		mortgageData.put(id, mortgage);
		return mortgage;
	}
	
	@RequestMapping(value = MortgageRestURIConstants.DELETE_MORTGAGE_RATE, method = RequestMethod.PUT)
	public @ResponseBody MortgageRateDetails deletemortgageRates(@PathVariable("id") int mortgageId) {
		logger.info("Start deletemortgageloyee.");
		MortgageRateDetails mortgage = mortgageData.get(mortgageId);
		mortgageData.remove(mortgageId);
		return mortgage;
	}
	
}

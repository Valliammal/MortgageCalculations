package com.mortgage.spring.controller;

public class MortgageRestURIConstants {

	public static final String GET_MORTGAGE_RATE = "/api/mortgage/{id}";
	public static final String GET_ALL_MORTGAGE_RATES = "/api/interest-rates";
	public static final String CREATE_MORTGAGE_RATE = "/api/mortgage/create/{id}?lastUpdate={lastUpdate}&maturityPeriod={maturityPeriod}&loanValue={loanValue}&interestRate={interestRate}";
	public static final String DELETE_MORTGAGE_RATE = "/api/mortgage/delete/{id}";
	public static final String CALCULATE_MORTGAGE_DETAILS = "/api/mortgage-check";
	
}

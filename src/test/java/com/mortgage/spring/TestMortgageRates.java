package com.mortgage.spring;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.mortgage.spring.controller.MortgageRestURIConstants;
import com.mortgage.spring.model.MortgageRateDetails;

public class TestMortgageRates {

	public static final String SERVER_URI = "http://localhost:8080/MortgageCalculations";
	
	public static void main(String args[]){
		
		System.out.println("*****");
		testCreateMortgageDetails();
		System.out.println("*****");
		testGetMortgage();
		System.out.println("*****");
		testGetAllMortgageRates();
	}

	private static void testGetAllMortgageRates() {
		RestTemplate restTemplate = new RestTemplate();
		//we can't get List<Employee> because JSON convertor doesn't know the type of
		//object in the list and hence convert it to default JSON object type LinkedHashMap
		List<LinkedHashMap> mortgages = restTemplate.getForObject(SERVER_URI+MortgageRestURIConstants.GET_ALL_MORTGAGE_RATES, List.class);
		System.out.println(mortgages.size());
		for(LinkedHashMap map : mortgages){
			System.out.println("ID="+map.get("id")+",Name="+map.get("name")+",CreatedDate="+map.get("createdDate"));;
		}
	}

	private static void testCreateMortgageDetails() {
		RestTemplate restTemplate = new RestTemplate();
		MortgageRateDetails mortgage = new MortgageRateDetails();
		MortgageRateDetails response = restTemplate.postForObject(SERVER_URI+MortgageRestURIConstants.CREATE_MORTGAGE_RATE, mortgage, MortgageRateDetails.class);
		printMortgageData(response);
	}

	private static void testGetMortgage() {
		RestTemplate restTemplate = new RestTemplate();
		MortgageRateDetails mortgage = restTemplate.getForObject(SERVER_URI+"/api/mortgage/1", MortgageRateDetails.class);
		printMortgageData(mortgage);
	}
	
	public static void printMortgageData(MortgageRateDetails mortgage){
		System.out.println("ID = " + mortgage.getId() + ",Last UpDate = " + mortgage.getLastUpdate());
	}
}
